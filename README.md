## Usage
The user needs to have .NET and Selenium Webdriver installed in his/her machine and preset the PATH variables accordingly.
In order to generate the report file, the user needs to open the solution and insert the desired path onto the "Hooks" class. (Hooks>Hooks.cs > line 70)

## Running the tests 

In order to run the tests, you can use the command:
```sh
dotnet test DeriveExercise
```

Or you can just open the solution and run it manually.

After that, the Chrome browser should open and the test script should run accordingly. When the test finishes, the more detailed report should be saved on the path informed by the user on the Hooks class.
## Description 

Decided to use SpecFlow in this solution, considering that it helps to organize and reuse code, used NUnit with .NET Core and Selenium to build the test scripts and ExtentReports to generate a pretty simple report which would be useful to demonstrate to a client/user how the test is doing.

## Nuget Packages used

| NuGet Package | Version |
| ------ | ------ |
| FluentAssertions | 5.10.3 |
| ExtentReports | 4.1.0 |
| Microsoft.NET.Test.SDK | 16.5.0 |
| NUnit3TestAdapter | 3.16.1 |
| Selenium.Support | 3.141.0 |
| Selenium.WebDriver | 3.141.0  |
| SpecFlow | 3.7.13 |
| SpecFlow.NUnit | 3.7.13 |
| SpecFlow.Tools.MsBuild.Generation | 3.7.13 |



