﻿
namespace DeriveExercise.Steps
{
    class ProductDetails
    {
        public string Name { get; set; }

        public string Color { get; set; }

        public string Size { get; set; }
    }
}
