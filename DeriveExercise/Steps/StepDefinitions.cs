﻿using DeriveExercise.Pages;
using FluentAssertions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace DeriveExercise.Steps
{
    [Binding]
    public class StepDefinitions
    {

        private DriverHelper _driverHelper;
        HomePage homePage;
        SearchPage searchPage;
        ProductDetailsPage productDetailsPage;
        CartPage cartPage;


        public StepDefinitions(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            homePage = new HomePage(_driverHelper.Driver);
            searchPage = new SearchPage(_driverHelper.Driver);
            productDetailsPage = new ProductDetailsPage(_driverHelper.Driver);
            cartPage = new CartPage(_driverHelper.Driver);
        }


        [Given(@"I navigate to the application")]
        public void GivenINavigateToTheApplication()
        {
            _driverHelper.Driver.Navigate().GoToUrl("http://automationpractice.com/");
        }

        [Given(@"Search and add these items to the cart")]
        public void GivenSearchAndAddTheseItemsToTheCart(Table table)
        {
            var productList = table.CreateSet<ProductDetails>();

            foreach(ProductDetails product in productList)
            {
                homePage.SearchProduct(product.Name);
                searchPage.SelectProduct(product.Name);
                productDetailsPage.SelectColor(product.Color);
                productDetailsPage.SelectSize(product.Size);
                productDetailsPage.ClickAddToCart();
                productDetailsPage.CloseProductAddedModal();
            }
        }

        [Given(@"Remove all items from shopping cart")]
        public void GivenRemoveAllItemsFromShoppingCart()
        {
            productDetailsPage.ClickCartPageButton();
            cartPage.RemoveAllProductsInCart();
        }


        [Then(@"The cart must have (.*) items in total")]
        public void ThenTheCartMustHaveItemsInTotal(int totalNumber)
        {
            productDetailsPage.ClickCartPageButton();
            cartPage.CheckTotalProductsInCart(totalNumber).Should().BeTrue();
        }

        [Then(@"The cart must be empty")]
        public void ThenTheCartMustBeEmpty()
        {
            productDetailsPage.ClickCartPageButton();
            cartPage.CheckIfCartIsEmpty().Should().BeTrue();
        }


    }
}
