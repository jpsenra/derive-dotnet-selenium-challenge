﻿Feature: ShoppingCart
	Check if the shopping cart is working correctly, adding and removing items according to user actions

Scenario: Check if cart is adding the correct number of items accordingly
	Given I navigate to the application
	And Search and add these items to the cart
		| Name |  Color | Size |
		| Blouse     | White | S |
		| Blouse     | Black | M |
		| Printed Dress  | Beige | M | 
		| Printed Dress  | Pink |L |
		| Printed Summer Dress  | Black | S |
		| Printed Summer Dress  | Orange | M |
		| Printed Summer Dress  | Blue | L |
		| Printed Summer Dress  | Yellow | S |
		| Printed Chiffon Dress  | Green | S |
		| Printed Chiffon Dress  | Yellow | L |
	Then The cart must have 10 items in total

Scenario: Check if cart is empty after removing items
	Given I navigate to the application
	And Search and add these items to the cart
		| Name |  Color | Size |
		| Blouse     | White | S |
		| Blouse     | Black | M |
		| Printed Dress  | Beige | M | 
		| Printed Dress  | Pink |L |
		| Printed Summer Dress  | Black | S |
		| Printed Summer Dress  | Orange | M |
		| Printed Summer Dress  | Blue | L |
		| Printed Summer Dress  | Yellow | S |
		| Printed Chiffon Dress  | Green | S |
		| Printed Chiffon Dress  | Yellow | L |
	And Remove all items from shopping cart
	Then The cart must be empty