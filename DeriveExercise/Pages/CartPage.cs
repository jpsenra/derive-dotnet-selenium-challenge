﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace DeriveExercise.Pages
{
    class CartPage
    {
        private IWebDriver Driver;

        IWebElement qtyOfProducts => Driver.FindElement(By.Id("summary_products_quantity"));

        IWebElement cartIsEmptyAlert => Driver.FindElement(By.XPath("//p[@class='alert alert-warning' and text()='Your shopping cart is empty.']"));

        public CartPage(IWebDriver driver)
        {
            Driver = driver;
        }

        public bool CheckTotalProductsInCart(int expectedQty)
        {
            var qtyText = qtyOfProducts.Text;
            string text = string.Empty;
            int qtyInt = 0;

            for(int i = 0; i<qtyText.Length; i++)
            {
                if (Char.IsDigit(qtyText[i]))
                    text += qtyText[i];
            }

             qtyInt = int.Parse(text);
            try
            {
                Assert.AreEqual(qtyInt, expectedQty);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool CheckIfCartIsEmpty()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.ElementToBeClickable(cartIsEmptyAlert));

            if (cartIsEmptyAlert.Enabled && cartIsEmptyAlert.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RemoveAllProductsInCart()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));

            IList<IWebElement> products = Driver.FindElements(By.XPath("//a[@title='Delete']"));
            for (int i =0; i < products.Count; i++)
            {
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//a[@title='Delete']")));
                products[i].Click();
            }
        }
    }
}
