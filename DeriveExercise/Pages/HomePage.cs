﻿using OpenQA.Selenium;

namespace DeriveExercise.Pages
{
    class HomePage 
    {
        private IWebDriver Driver;

        public HomePage(IWebDriver driver)
        {
            Driver = driver;
        }

        IWebElement lnkWomen => Driver.FindElement(By.LinkText("Women"));

        IWebElement lnkDresses => Driver.FindElement(By.LinkText("Dresses"));

        IWebElement lnkTshirts => Driver.FindElement(By.LinkText("T-shirts"));

        IWebElement inputSearch => Driver.FindElement(By.Id("search_query_top"));

        IWebElement btnSearch => Driver.FindElement(By.Name("submit_search"));

        IWebElement btnCartPage => Driver.FindElement(By.XPath("//a[@title='View my shopping cart']"));

        public void ClickWomen() => lnkWomen.Click();

        public void SearchProduct(string product) 
        {
            inputSearch.Clear();
            inputSearch.SendKeys(product);
            btnSearch.Click();
        }

        public void ClickCartPageButton()
        {
            btnCartPage.Click();
        }


    }
}
