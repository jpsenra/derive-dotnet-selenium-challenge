﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace DeriveExercise.Pages
{
    class ProductDetailsPage
    {
        private IWebDriver Driver;

        public ProductDetailsPage(IWebDriver driver)
        {
            Driver = driver;
        }

        IWebElement btnAddToCart => Driver.FindElement(By.XPath("//p[@id='add_to_cart']/button[@name='Submit']"));

        IWebElement btnCloseAddedModal => Driver.FindElement(By.XPath("//span[@title='Close window']"));

        IWebElement btnCartPage => Driver.FindElement(By.XPath("//a[@title='View my shopping cart']"));

        IWebElement selectSize => Driver.FindElement(By.Id("group_1"));


        public void ClickAddToCart()
        {
            btnAddToCart.Click();
        }

        public void CloseProductAddedModal()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnCloseAddedModal));
            btnCloseAddedModal.Click();
        }

        public void SelectColor(string productColor)
        {
            Driver.FindElement(By.XPath("//li/a[@name = '" + productColor + "']")).Click();
        }

        public void SelectSize(string size)
        {
            var selectElement = new SelectElement(selectSize);
            selectElement.SelectByText(size);
        }

        public void ClickCartPageButton()
        {
            btnCartPage.Click();
        }

    }
}
