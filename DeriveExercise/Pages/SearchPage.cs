﻿using OpenQA.Selenium;


namespace DeriveExercise.Pages
{
    class SearchPage
    {

        private IWebDriver Driver;

        public SearchPage(IWebDriver driver)
        {
            Driver = driver;
        }

        public void SelectProduct(string productName)
        {
            Driver.FindElement(By.XPath("(//div[@class='product-container']//a[@class='product-name' and @title = '" + productName + "'])[1]")).Click();

        }

    }
}
